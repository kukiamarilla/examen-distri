package com.distri.server.models;

import java.util.Objects;

/**
 * Persona
 */
public class Persona {

    private Integer id;
    private String nombre;
    private String apellido;
    private String chapa;
    private String marca;


    public Persona() {
    }

    public Persona(Integer id, String nombre, String apellido, String chapa, String marca) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.chapa = chapa;
        this.marca = marca;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getChapa() {
        return this.chapa;
    }

    public void setChapa(String chapa) {
        this.chapa = chapa;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Persona id(Integer id) {
        this.id = id;
        return this;
    }

    public Persona nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public Persona apellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public Persona chapa(String chapa) {
        this.chapa = chapa;
        return this;
    }

    public Persona marca(String marca) {
        this.marca = marca;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Persona)) {
            return false;
        }
        Persona persona = (Persona) o;
        return Objects.equals(id, persona.id) && Objects.equals(nombre, persona.nombre) && Objects.equals(apellido, persona.apellido) && Objects.equals(chapa, persona.chapa) && Objects.equals(marca, persona.marca);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellido, chapa, marca);
    }


}