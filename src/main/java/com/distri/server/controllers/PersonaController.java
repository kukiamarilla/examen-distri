package com.distri.server.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.distri.server.models.Persona;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * PersonaController
 */
public class PersonaController {

    public static HashMap<Integer, Persona> personas = new HashMap<>();
    public static Integer idCounter = 0;

    public static String registrar(String payload) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Persona persona = mapper.readValue(payload, Persona.class);
            Integer id = ++PersonaController.idCounter;
            persona.setId(id);
            PersonaController.personas.put(id, persona);
            return mapper.writeValueAsString(persona);
        } catch (JsonMappingException e) {
            System.err.println("Campos incorrectos en: " + payload);
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            System.err.println("Sintaxis incorrecta en: " + payload);
            e.printStackTrace();
        }
        return "{\"status\": \"error\", \"message\": \"No se pudo completar la consulta.\"}";

    }

    public static String listar() {
        ObjectMapper mapper = new ObjectMapper();
        List<Persona> personasList = new ArrayList<Persona>(personas.values());
        try {
            return mapper.writeValueAsString(personasList);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.err.println("Sintaxis incorrecta");
            e.printStackTrace();
        }
        return "{\"status\": \"error\", \"message\": \"No se pudo completar la consulta.\"}";
    }
}
