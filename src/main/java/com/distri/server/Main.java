package com.distri.server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) throws IOException {
        ServerSocket socket = null;
        boolean listening = true;
        Router.register();
        try {
            socket = new ServerSocket(4444);
        } catch (IOException e) {
            System.err.println("No se pudo crear el servidor");
            System.exit(1);
        }

        System.out.println("Puerto abiero: 4444");

        while (listening) {
            new ServerThread(socket.accept()).start();
        }

        socket.close();
    }
}