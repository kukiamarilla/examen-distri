package com.distri.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.json.JSONObject;

/**
 * ServerThread
 */
public class ServerThread extends Thread {

    Socket socket;

    public ServerThread(Socket socket) {
        super("ServerThread");
        this.socket = socket;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        super.run();
        try {
            PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            String inputLine, outputLine;

            while ((inputLine = in.readLine()) != null) {
                System.out.println("Recibido: " + inputLine);
                if (inputLine.equals("Bye")) {
                    break;
                }
                JSONObject request = new JSONObject(inputLine);
                String operation = request.getString("operation");
                if(request.has("payload")){
                    String payload = request.get("payload").toString();
                    System.out.println("Payload: " + payload);
                    outputLine = Router.execute(operation, payload);
                }else{
                    outputLine = Router.execute(operation);
                }
                out.println(outputLine);
                System.out.println("Enviado: " + outputLine);
            }

            out.close();
            in.close();
            socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}