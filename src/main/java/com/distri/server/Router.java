package com.distri.server;

import java.lang.reflect.Method;
import java.util.HashMap;

public class Router {

    private static HashMap<String, String> routes;

    public static void register() {
        routes = new HashMap<>();
        routes.put("add", "PersonaController@registrar");
        routes.put("listar", "PersonaController@listar");
    }

    public static String execute(String operation, String payload) {
        String controller[] = Router.routes.get(operation).split("@");
        String claseNombre = controller[0];
        String metodoNombre = controller[1];
        try {
            Method metodo = Class.forName("com.distri.server.controllers." + claseNombre).getMethod(metodoNombre,
                    String.class);
            return (String) metodo.invoke(null, payload);
        } catch (Exception e) {
            System.err.println("Error inesperado al ejecutar el controlador");
            e.printStackTrace();
        }
        return null;
    }

    public static String execute(String operation) {
        String controller[] = Router.routes.get(operation).split("@");
        String claseNombre = controller[0];
        String metodoNombre = controller[1];
        try {
            Method metodo = Class.forName("com.distri.server.controllers." + claseNombre).getMethod(metodoNombre);
            return (String) metodo.invoke(null);
        } catch (Exception e) {
            System.err.println("Error inesperado al ejecutar el controlador");
            e.printStackTrace();
        }
        return null;
    }

}
