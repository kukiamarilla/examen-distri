package com.distri.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;

import com.distri.server.models.Persona;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import org.json.JSONObject;

public class Main {

    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            Boolean ingresando = true;
            Integer operacion;
            while (ingresando) {

                do {
                    System.out.println("Ingrese el numero de operación (1. Registrar, 2. Listar): ");
                    operacion = scanner.nextInt();
                    scanner.nextLine();
                    if (operacion != 1 && operacion != 2) {
                        System.out.println("No existe ese numero de operación");
                    }
                } while (operacion != 1 && operacion != 2);

                switch (operacion) {
                    case 1:
                        registrar(scanner);
                        break;
                    case 2:
                        listar();
                        break;
                }
                System.out.println("Desea ingresar otra operación (S/N): ");
                String respuesta = scanner.nextLine();
                if (respuesta.equals("N"))
                    ingresando = false;
            }

            scanner.close();

        } catch (Exception e) {
            System.err.println("Hubo problemas al procesar la entrada");
            e.printStackTrace();
        }

    }

    public static String serverCall(JSONObject request) throws UnknownHostException, IOException {
        Socket socket = new Socket("localhost", 4444);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        String fromServer;
        out.println(request.toString());
        System.out.println();
        System.out.println("Enviado: " + request.toString());
        fromServer = in.readLine();
        System.out.println("Recibido: " + fromServer);
        System.out.println();
        out.println("Bye");

        out.close();
        in.close();
        socket.close();
        return fromServer;
    }

    public static void registrar(Scanner scanner) {
        try {
            System.out.println("Ingrese el nombre: ");
            String nombre = scanner.nextLine();
            System.out.println("Ingrese el apellido: ");
            String apellido = scanner.nextLine();
            System.out.println("Ingrese la chapa: ");
            String chapa = scanner.nextLine();
            System.out.println("Ingrese la marca: ");
            String marca = scanner.nextLine();
            JSONObject request = new JSONObject();
            JSONObject payload = new JSONObject();
            request.put("operation", "add");
            payload.put("nombre", nombre);
            payload.put("apellido", apellido);
            payload.put("chapa", chapa);
            payload.put("marca", marca);
            request.put("payload", payload);
            serverCall(request);
            System.out.println();
        } catch (Exception e) {
            System.err.println("No se pudo realizar la consulta");
        }
    }

    public static void listar() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JSONObject request = new JSONObject();
            request.put("operation", "listar");
            String respuesta = serverCall(request);
            List<Persona> personas = mapper.readValue(respuesta, (new TypeReference<List<Persona>>() {
            }));
            for (int i = 0; i < personas.size(); i++) {
                Persona persona = personas.get(i);
                System.out.println("Id: " + persona.getId());
                System.out.println("Nombre: " + persona.getNombre());
                System.out.println("Apellido: " + persona.getApellido());
                System.out.println("Chapa: " + persona.getChapa());
                System.out.println("Marca: " + persona.getMarca());
                System.out.println();
            }

        } catch (Exception e) {
            System.err.println("No se pudo realizar la consulta");
        }
    }
}
