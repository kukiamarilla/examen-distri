# Practica del Examen de Sistemas Distribuidos

## Descripción

Servidor de registro de personas y autos

## Clases

- `com.distri.cliente.Main`: Esta clase es el Entry Point del programa cliente, se encarga de abrir un socket y manejar la lógica de negocios y la comunación con el servidos.

- `com.distri.server.Main`: Es el Entry pont del servidor. Esta clase inicializa el servidor y recibe las conexiones de los clientes.

- `com.distri.server.ServerThread`: Esta clase maneja una conexión con un cliente específico y maneja las consultas de ese cliente.

- `com.distri.server.Router`: Se encarga de direccionar el flujo de procesos a los metodos del controlador según el código de operación en la consulta del cliente.

- `com.distri.server.controllers.PersonaController`: Controlador del recurso Persona. Se encarga de ejecutar las operaciones para Personas.

- `com.distri.server.models.Persona`: Modelo de Persona. Define la estructura de un Persona
